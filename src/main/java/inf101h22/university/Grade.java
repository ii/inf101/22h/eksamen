package inf101h22.university;

/**
 * Letter grade.
 * A - E are represented by the integer values 5 - 1, while FAIL is represented by -1. FAIL is not used when calculating grade point average.
 */
public enum Grade {
    
    A(5),
    B(4),
    C(3),
    D(2),
    E(1),
    FAIL(-1);

    public final int numberRepresentation;

    private Grade(int numberRepresentation) {
        this.numberRepresentation = numberRepresentation;
    }
}
