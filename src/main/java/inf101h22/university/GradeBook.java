package inf101h22.university;

import java.util.ArrayList;
import java.util.List;

/**
 * This class holds a list of grades and a value for grade point average.
 * Grade point average (GPA) is a decimal value, which is the average of all letter grades represented by integer values. 
 * If you have the grades: A, E, B, B, C your GPA is (5 + 1 + 4 + 4 + 3) / 5 = 3.4
 * If you have the grades: E, D, E, B, F (FAIL) your GPA is (1 + 2 + 1 + 4) / 4 = 2.0. Note that F is not included in the calculation of GPA.
 * If you have the grades: F, F, F, F, F your GPA is 0.0.
 */
public class GradeBook {
    
    private double gpa = 0.0;
    private List<Grade> grades;

    public GradeBook() {
        grades = new ArrayList<>();
    }

    public GradeBook(List<Grade> grades) {
        this.grades = grades;
        calculateGPA();
    }

    /**
     * Calculates the grade point average of all grades.
     * The grade FAIL should not be included in the calculation.
     */
    private void calculateGPA() {
        double sumValue = 0;
        int numberOfPasses = 0;
        for (Grade grade : this.grades) {
            if (!grade.equals(Grade.FAIL)) {
                sumValue += grade.numberRepresentation;
                numberOfPasses += 1;
            }
        }
        this.gpa = sumValue / Math.max(1, numberOfPasses);
    }

    public void add(Grade grade) {
        grades.add(grade);
        calculateGPA();
    }

    public double getGpa() {
        return gpa;
    }
}
