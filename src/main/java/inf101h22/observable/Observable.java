package inf101h22.observable;

/**
 * An Observable is an object that allows the addition and removal of
 * an observer method to the object. A class which implements the
 * Observer interface is expected to make a call to the added observers
 * every time its core attributes are mutated.
 */
public interface Observable {

    /**
     * Adds a (non-null) observer. The {@link Observer#update update}
     * method on the added observer will be called every time the value
     * of the observed variable changes. It will not be called
     * immediately on the observer being installed, only when the
     * variable changes. To get the current value, use the
     * {@link #getValue} method.
     * 
     * @param observer to be added.
     */
    public void addObserver(Observer observer);

    /**
     * Removes a (non-null) observer.
     * 
     * @param observer to be removed.
     * @return true if the observer was removed, and false if the observer was not
     *         found.
     */
    public boolean removeObserver(Observer observer);

}
