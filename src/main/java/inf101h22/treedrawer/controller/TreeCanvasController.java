package inf101h22.treedrawer.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

import javax.swing.JToggleButton;
import javax.swing.Timer;

import inf101h22.treedrawer.model.TreeCanvas;
import inf101h22.treedrawer.view.MainView;
import inf101h22.treedrawer.view.TreeCanvasView;

/** 
 * The controller class for the TreeDrawer application. The TreeCanvasController is
 * the main controller class, responsible for handling any user input, such as
 * mouse events, whilst also responsible for automatic timed events.
 * 
 * TODO:
 * If this were to grow considerably, consider refactoring it into separate classes
 */
public class TreeCanvasController implements MouseListener {

    // Field variables referencing model and view
    private TreeCanvasView treeCanvasView;
    private TreeCanvas model;
    private JToggleButton autoButton;

    // Field variables managed by the controller
    private Timer timer = new Timer(1000, this::clockTick);;
    private Random random = new Random();

    /** Creates a new TreeCanvasController for the provided model and view */
    public TreeCanvasController(TreeCanvas model, MainView view) {
        this.model = model;
        this.treeCanvasView = view.getTreeCanvasView();
        this.treeCanvasView.addMouseListener(this);
        view.getResetButton().addActionListener(this::resetPressed);
        view.getUndoButton().addActionListener(this::undoPressed);

        this.autoButton = view.getAutoButton();
        this.autoButton.addItemListener(this::autoButtonToggle);
        this.timer.setInitialDelay(0);
    }

    private void resetPressed(ActionEvent event) {
        this.model.clear();
    }

    private void undoPressed(ActionEvent event) {
        this.model.undo();
    }

    private void clockTick(ActionEvent e) {
        double x = random.nextDouble();
        double y = random.nextDouble();
        this.model.addNode(x, y);
    }

    private void autoButtonToggle(ItemEvent event) {
        int newState = event.getStateChange();
        if (newState == 1) {
            this.timer.start();
        }
        else {
            this.timer.stop();
        }
    }

    @Override
    public void mousePressed(MouseEvent event) {
        double x = event.getX() * 1.0 / this.treeCanvasView.getWidth();
        double y = event.getY() * 1.0 / this.treeCanvasView.getHeight();
        this.model.addNode(x, y);
    }

    @Override public void mouseClicked(MouseEvent event)  { /* ignored */ }
    @Override public void mouseEntered(MouseEvent event)  { /* ignored */ }
    @Override public void mouseExited(MouseEvent event)   { /* ignored */ }
    @Override public void mouseReleased(MouseEvent event) { /* ignored */ }
}
