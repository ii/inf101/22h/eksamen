package inf101h22.treedrawer.model;

import inf101h22.list.ObservableList;
import inf101h22.observable.ObservableValue;

public interface ReadableTreeCanvas {

    /**
     * Gets the nodes in the tree.
     * @return an observable list of the nodes in the tree.
     */
    ObservableList<Node> getNodes();

    /**
     * Gets the status message of the TreeCanvas.
     * @return the status message wrapped in an observable object.
     */
    ObservableValue<String> getMessage();
}
