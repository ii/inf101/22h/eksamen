package inf101h22.university;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UniversityClassTest {

    IClass theClass;

    @BeforeEach
    public void init() {
        // uncomment the line below to test UniversityClass
        this.theClass = new UniversityClass(); 
    }
    
    @Test
    public void testAveGpa() {
        // Student #1 with grades A, B, C (gpa: 4.0)
        Student student1 = new Student(1, "Beta");
        for (Grade grade : Arrays.asList(Grade.A, Grade.B, Grade.C)) {
            student1.getGrades().add(grade);
        }
        this.theClass.add(student1);

        // Student #2 with grades C, C (gpa: 3.0)
        Student student2 = new Student(2, "Charlie");
        for (Grade grade : Arrays.asList(Grade.C, Grade.C)) {
            student2.getGrades().add(grade);
        }
        this.theClass.add(student2);

        // The test
        double expectedAverageGpa = 3.5;
        assertEquals(expectedAverageGpa, theClass.getAverageGpa());
    }

    @Test
    public void testAddAndSize() { 
        assertEquals(0, theClass.size());
        theClass.add(new Student(0, "Alpha"));
        assertEquals(1, theClass.size());
        theClass.add(new Student(1, "Beta"));
        assertEquals(2, theClass.size());
    }
}
